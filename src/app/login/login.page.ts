import { Component, OnInit } from '@angular/core';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';

const IMAGE_DIR = 'stored-images';
 
interface LocalFile {
  name: string;
  path: string;
  data: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  myForm: FormGroup;
  token = 'ivhsduuhceg';
  images: LocalFile[] = [];
  image: string;
  path: string;
  constructor(
    private router: Router, 
    private formBuilder: FormBuilder, 
    public toastController: ToastController,
    private plt: Platform,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController
  ) { 
    this.loadFiles();    
  }

  ngOnInit() {
    this.myForm = this.formBuilder.group({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });

    
  }

  login(){
    const users: any = JSON.parse(localStorage.getItem('users'));
    if(users){
      var userValidation = false;
      for (let i = 0; i < users.length; i++) {
        if(users[i].username == this.myForm.value.username && users[i].password == this.myForm.value.password){
          userValidation = true;
        }
      }
      if(userValidation){
        localStorage.setItem('token', this.token);
        this.router.navigate(['/home']);
      }else{
        this.invalidAlert('Username or password incorrect');  
      }
    }else{
      this.invalidAlert('Please register');
    }
    
  }

  async invalidAlert(text) {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }

  async loadFiles() {
    this.images = [];
 
    const loading = await this.loadingCtrl.create({
      message: 'Loading data...',
    });
    await loading.present();
 
    Filesystem.readdir({
      path: IMAGE_DIR,
      directory: Directory.Data,
    }).then(result => {
      this.loadFileData(result.files);
    },
      async (err) => {
        // Folder does not yet exists!
        await Filesystem.mkdir({
          path: IMAGE_DIR,
          directory: Directory.Data,
        });
      }
    ).then(_ => {
      loading.dismiss();
    });
  }

  async loadFileData(fileNames: string[]) {
    for (let f of fileNames) {
      const filePath = `${IMAGE_DIR}/${f}`;
 
      const readFile = await Filesystem.readFile({
        path: filePath,
        directory: Directory.Data,
      });
      
      this.image = `data:image/jpeg;base64,${readFile.data}`;
      this.path = filePath;
    }
  }


  async selectImage() {
    const image = await Camera.getPhoto({
        quality: 90,
        allowEditing: false,
        resultType: CameraResultType.Uri,
        source: CameraSource.Photos
    });
    
    
    if (image) {
        this.saveImage(image)
    }
  }

  async saveImage(photo: Photo) {
    if(this.image){
      await Filesystem.deleteFile({
        directory: Directory.Data,
        path: this.path
      });
    }
    

    const base64Data = await this.readAsBase64(photo);
 
    const fileName = new Date().getTime() + '.jpeg';    

    const savedFile = await Filesystem.writeFile({
        path: `${IMAGE_DIR}/${fileName}`,
        data: base64Data,
        directory: Directory.Data
    }); 
 
    this.loadFiles();
  }

  private async readAsBase64(photo: Photo) {
    if (this.plt.is('hybrid')) {
        const file = await Filesystem.readFile({
            path: photo.path
        });
 
        return file.data;
    }
    else {
        const response = await fetch(photo.webPath);
        const blob = await response.blob();
 
        return await this.convertBlobToBase64(blob) as string;
    }
  }

  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
        resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

}
