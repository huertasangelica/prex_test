import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormControl  } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-movie',
  templateUrl: './edit-movie.page.html',
  styleUrls: ['./edit-movie.page.scss'],
})
export class EditMoviePage implements OnInit {
  myForm: FormGroup;
  id: any;
  movies: any = [];
  token: string;
  title: string;
  intro: string;
  description: string;
  constructor(private formBuilder: FormBuilder,private activatedRoute: ActivatedRoute,private router: Router) { 
  }

  ngOnInit() {
    this.myForm = this.formBuilder.group({
      title: new FormControl('', Validators.required),
      intro: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });

    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    
    this.movies = JSON.parse(localStorage.getItem('movies'));
    this.token = localStorage.getItem('token');
    this.title = this.movies[this.id].title;
    this.intro = this.movies[this.id].intro;
    this.description = this.movies[this.id].description;
  
    this.myForm.controls['title'].setValue(this.title);
    this.myForm.controls['description'].setValue(this.description);
    this.myForm.controls['intro'].setValue(this.intro);
  }

  saveData(){
    if (this.myForm.valid){
      this.movies[this.id].title = this.myForm.value.title;
      this.movies[this.id].intro = this.myForm.value.intro;
      this.movies[this.id].description = this.myForm.value.description;
      
      localStorage.removeItem('movies');
      localStorage.setItem('movies',JSON.stringify(this.movies));
      this.router.navigate(['/movie-detail', this.id])
    }
  }

}
