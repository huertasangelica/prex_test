import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  token: any;
  public appPages = [
    { title: 'Movies', url: '/home', icon: 'film' }
  ];
  constructor(private router: Router,private menu: MenuController) {}

  ngOnInit() {
    this.token = localStorage.getItem('token');
    if(!this.token){
      const login = { title: 'Login', url: '/login', icon: 'log-in' };
      this.appPages.push(login);
    }
  }

  ionViewWillEnter(){
    this.token = localStorage.getItem('token');
    if(!this.token){
      const login = { title: 'Login', url: '/login', icon: 'log-in' };
      this.appPages.push(login);
    } 
  }

  logout(){
    localStorage.removeItem('token');
    this.token = null;
    this.menu.close();
    const login = { title: 'Login', url: '/login', icon: 'log-in' };
    this.appPages.push(login)
    this.router.navigate(['/home'])
  }
}
