import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  movies: any = [];
  rate: any = [1,2,3,4,5];
  moviesLocal: any = []
  constructor(private http: HttpClient) {   
  }

  ngOnInit() {
    this.moviesLocal = JSON.parse(localStorage.getItem('movies'));
    this.movies = this.moviesLocal;
    if(!this.moviesLocal){
      this.getMovies().subscribe(res =>{
        this.movies = res;
        localStorage.setItem('movies',JSON.stringify(res));
      });
    }
    
  }

  ionViewWillEnter(){
    this.moviesLocal = JSON.parse(localStorage.getItem('movies'));
    this.movies = this.moviesLocal;
    
  }

  getMovies(){
    return this.http.get('assets/data/movies.json').pipe(
      
      map((res: any) =>{
        return res.data;
      })
    )
  }
  
}
