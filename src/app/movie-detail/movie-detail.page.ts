import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.page.html',
  styleUrls: ['./movie-detail.page.scss'],
})
export class MovieDetailPage implements OnInit {
  id: any;
  movies: any = [];
  title: string;
  description: string;
  rateM: number;
  rate: any = [1,2,3,4,5];
  token: any;
  
  constructor(private activatedRoute: ActivatedRoute, private http: HttpClient, private router: Router,public alertController: AlertController) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    
    this.movies = JSON.parse(localStorage.getItem('movies'));
    this.token = localStorage.getItem('token');
    console.log(this.token);
    this.title = this.movies[this.id].title;
    this.description = this.movies[this.id].description;
    this.rateM = this.movies[this.id].rate;
    
  }

  ionViewWillEnter(){ 
    this.movies = JSON.parse(localStorage.getItem('movies'));
    this.title = this.movies[this.id].title;
    this.description = this.movies[this.id].description;
    this.rateM = this.movies[this.id].rate;
    
  }

  rateMovie(rate){
    this.rateM = rate + 1;
    this.movies[this.id].rate = this.rateM;

    localStorage.removeItem('movies');
    localStorage.setItem('movies',JSON.stringify(this.movies));
    
  }

  async deleteMovie() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      message: 'Are you sure you want to delete the movie?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Delete',
          handler: () => {
            this.movies.splice(this.id, 1);
            localStorage.removeItem('movies');
            localStorage.setItem('movies',JSON.stringify(this.movies));
            this.router.navigate(['/home'])
          }
        }
      ]
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    //this.router.navigate(['/edit-movie', this.id])
  }

  
}
