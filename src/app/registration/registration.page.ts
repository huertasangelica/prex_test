import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
  myForm: FormGroup;
  constructor(private router: Router, private formBuilder: FormBuilder,public toastController: ToastController) { }

  ngOnInit() {
    this.myForm = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      password2: new FormControl('', Validators.required),
    });
  }

  signIn(){
    if (this.myForm.valid){
      if(this.myForm.value.password == this.myForm.value.password2){
        
        const users: any = JSON.parse(localStorage.getItem('users'));
        if(users){
          users.push(this.myForm.value);
        }else{
          const newUsers = [this.myForm.value];
          localStorage.setItem('users', JSON.stringify(newUsers));
        }
        this.invalidAlert('Registered successfully');
        this.router.navigate(['/login']);
      }else{
        this.invalidAlert('Please, check again the password confirmation');
      }
    }else{
      this.invalidAlert('Please, complete the entire form');
    }
    
  }

  async invalidAlert(text) {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }
}
